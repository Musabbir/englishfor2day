package com.musabbir.englishfor2day;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

//If you want Material design Title bar then  you should extends AppCompatActivity
public class MainActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {

    LinearLayout child_layout, video_lec, grammar, gre, spoken, article, all, others, saved_words, faq;
    ImageView spokenImgid;
    Intent intent;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        child_layout = (LinearLayout) findViewById(R.id.child_layout);
        spoken = (LinearLayout) findViewById(R.id.spoken);
        video_lec = (LinearLayout) findViewById(R.id.video_lec);
        grammar = (LinearLayout) findViewById(R.id.grammar);
        gre = (LinearLayout) findViewById(R.id.gre);
        article = (LinearLayout) findViewById(R.id.article);
        all = (LinearLayout) findViewById(R.id.all);
        others = (LinearLayout) findViewById(R.id.others); //others replaced by Manners & Dialogs
        saved_words = (LinearLayout) findViewById(R.id.saved_words);
        faq = (LinearLayout) findViewById(R.id.faq);

        child_layout.setOnClickListener(this);
        spoken.setOnClickListener(this);
        video_lec.setOnClickListener(this);
        grammar.setOnClickListener(this);
        gre.setOnClickListener(this);
        article.setOnClickListener(this);
        all.setOnClickListener(this);
        others.setOnClickListener(this);
        saved_words.setOnClickListener(this);
        faq.setOnClickListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.visit_us) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://englishfor2day.com"));
            startActivity(intent);
        } else if (id == R.id.facebook_page) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/Englishfor2day"));
            startActivity(intent);
        } else if (id == R.id.youtube_channel) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/channel/UCrP1LVG6TVUq3b8k8aXPSfA"));
            startActivity(intent);
        } else if (id == R.id.rate_app) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.musabbir.englishfor2day"));
            startActivity(intent);
        } else if (id == R.id.share_app) {
            intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Englishfor2day");
            intent.putExtra(android.content.Intent.EXTRA_TEXT, "englishfor2day.com is an educational website for learning English language and grammar properly.. \n Visit http://englishfor2day.com/ for more.");
            startActivity(Intent.createChooser(intent, "Share via"));
        } else if (id == R.id.about) {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.about);
            TextView text = (TextView) dialog.findViewById(R.id.text);
            dialog.show();
        } else if (id == R.id.feedback) {
            Intent email = new Intent(Intent.ACTION_SEND);
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{"shamim.ce38@gmail.com"});
            email.putExtra(Intent.EXTRA_SUBJECT, "Feedback to Englishfor2day");
            email.setType("message/rfc822");
            startActivityForResult(Intent.createChooser(email, "Choose an email client:"), 1);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.child_layout:
                openSubMenuActivity("child");
                break;
            case R.id.spoken:
                spokenActivity("http://www.englishfor2day.com/feed/spoken.json", "Spoken"); //Grammar & Spoken both showed on same submenu + Article description
                break;
            case R.id.video_lec:
                openSubMenuActivity("video_lec");
                break;
            case R.id.grammar:
                spokenActivity("http://www.englishfor2day.com/feed/grammar.json", "Grammar"); //Grammar & Spoken both showed on same submenu + Article description
                break;
            case R.id.gre:
                openSubMenuActivity("gre");
                break;
            case R.id.article:
                openSubMenuActivity("article");
                break;
            //All menu is changed to Manners and Dialogue
            case R.id.all:
                openSubMenuActivity("all");
                break;
            //Others menu is changed to snippet
            case R.id.others:
                openSubMenuActivity("others");
                break;
            case R.id.saved_words:
                openSubMenuActivity("saved_words");
                break;
            case R.id.faq:
//                openItemActivity("http://www.englishfor2day.com/feed/faq.json", subMenuText);
                Toast.makeText(MainActivity.this, "Development in progress", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void openSubMenuActivity(String menu_name) {
        SavedWordsDbOperations swddbObj = new SavedWordsDbOperations(getBaseContext());
        if (swddbObj.getAllSubgaegory().isEmpty() && menu_name.equals("saved_words")) {
            TastyToast.makeText(getApplicationContext(), "You didn't save any word", TastyToast.LENGTH_LONG, TastyToast.ERROR);
        } else if (menu_name.equals("video_lec")) {
            Intent i = new Intent(MainActivity.this, Videos.class);
            i.putExtra("sub_menu_name", "Video lectures");
            i.putExtra("sub_menu_url", "http://www.englishfor2day.com/feed/spokenvideo.json");
            startActivity(i);
        } else if (menu_name.equals("all")) {
            Intent i = new Intent(MainActivity.this, Items.class);
            i.putExtra("sub_menu_name", "Manners and Dialogue");
            i.putExtra("sub_menu_url", "http://englishfor2day.com/feed/manners-and-dialogue.json");
            startActivity(i);
        } else {
            Intent i = new Intent(MainActivity.this, SubMenu.class);
            i.putExtra("menu_name", menu_name);
//            overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
            startActivity(i);
        }
    }

    public void spokenActivity(String sub_menu_url, String sub_menu_name) {
        Intent i;
        i = new Intent(MainActivity.this, Spoken.class);
        i.putExtra("sub_menu_url", sub_menu_url);
        i.putExtra("sub_menu_name", sub_menu_name);
        startActivity(i);
    }
}
