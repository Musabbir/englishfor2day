package com.musabbir.englishfor2day;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Musabbir on 30-Aug-16.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "engfortwoday.db";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE_SAVED_WORDS = "CREATE TABLE " + SavedWordsTable.TABLE + "("
                + SavedWordsTable.KEY_id + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + SavedWordsTable.KEY_subcategory + " TEXT, "
                + SavedWordsTable.KEY_name + " TEXT, "
                + SavedWordsTable.KEY_meaning + " TEXT,"
                + SavedWordsTable.KEY_img + " TEXT,"
                + SavedWordsTable.KEY_text1 + " TEXT,"
                + SavedWordsTable.KEY_text2 + " TEXT,"
                + SavedWordsTable.KEY_text3 + " TEXT)";
        db.execSQL(CREATE_TABLE_SAVED_WORDS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + SavedWordsTable.TABLE);
        onCreate(db);
    }
}