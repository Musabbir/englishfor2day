package com.musabbir.englishfor2day;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.musabbir.englishfor2day.adapter.ItemAdapter;
import com.musabbir.englishfor2day.model.ItemModel;
import com.sdsmdg.tastytoast.TastyToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by MUSABBIR MAMUN on 26-Jul-16.
 */

public class Items extends Activity {

    private List<ItemModel> itemList = new ArrayList<ItemModel>();
    public ListView listView;
    private ItemAdapter adapter;
    TextToSpeech tts;
    private ProgressDialog pDialog;
    final Context context = this;
    JSONObject json;
    String jsonResponse = "";
    public static String url_items = "", sub_menu_name = "";
    EditText inputSearch;
    TextView itemsHeader, antonym;
    String str = "", custom_img_url = "";
    String searchStr;
    ImageView search_img;
    private int MY_SOCKET_TIMEOUT_MS  = 15000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.items);

        Bundle extras = getIntent().getExtras();
        url_items = extras.getString("sub_menu_url");
        sub_menu_name = extras.getString("sub_menu_name");

        inputSearch = (EditText) findViewById(R.id.inputSearch);
        search_img = (ImageView) findViewById(R.id.search_img);
        itemsHeader = (TextView) findViewById(R.id.itemsHeader);
        antonym = (TextView) findViewById(R.id.antonym);

        itemsHeader.setText(sub_menu_name);

        listView = (ListView) findViewById(R.id.listView);

        //The last parameter getBaseContext() is so important here to send the Application context to ItemsAdapter class
        adapter = new ItemAdapter(this, itemList, getBaseContext());
        listView.setAdapter(adapter);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView meaning = (TextView) view.findViewById(R.id.meaning);
                str = meaning.getText().toString();
                TastyToast.makeText(getApplicationContext(), "This word saved to your list", TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                return true;
            }
        });

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");

        if (sub_menu_name.equals("Form of verbs")) {
            makeJsonArrayRequestFormOfVerbs(url_items);
        } else if (sub_menu_name.equals("Idioms and Phrase") || sub_menu_name.equals("Proverb")) {
            makeJsonArrayRequestPhrasesIdiomsProbverb(url_items);
        } else {
            makeJsonArrayRequest(url_items);
        }

        search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (itemsHeader.getVisibility() == View.VISIBLE) {
                    itemsHeader.setVisibility(View.GONE);
                    inputSearch.setVisibility(View.VISIBLE);
                    inputSearch.requestFocus();
                    InputMethodManager imm = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(inputSearch,
                            InputMethodManager.SHOW_IMPLICIT);
                } else {
                    itemsHeader.setVisibility(View.VISIBLE);
                    inputSearch.setVisibility(View.GONE);
                    inputSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(
                            inputSearch.getWindowToken(), 0);
                }
            }
        });

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.getFilter().filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                searchStr = inputSearch.getText().toString();
//                if (searchStr.matches("")) {
//                    Intent intent = getIntent();
//                    finish();
//                    startActivity(intent);
//                    adapter = new ItemAdapter(context,itemList);
//                    listView.setAdapter(adapter);
//                    pDialog.setMessage("Please wait...");
//                    makeJsonArrayRequest(url_items);
//                }
            }
        });
    }

    //This array request is work for Child, GRE, & All MENUS
    private void makeJsonArrayRequest(String url_items) {
        showpDialog();
        JsonArrayRequest req = new JsonArrayRequest(url_items,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            jsonResponse = "";
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject obj = (JSONObject) response.get(i);

                                ItemModel itemModelObj = new ItemModel();
                                if (obj.getString("name").equals("") || obj.getString("name").equals(null)) {
                                    itemModelObj.setName("N/A");
                                } else {
                                    itemModelObj.setName(obj.getString("name"));
                                }

                                if (obj.getString("e_name").equals("") || obj.getString("e_name").equals(null)) {
                                    itemModelObj.setE_name("N/A");
                                } else {
                                    itemModelObj.setE_name("Eng. name: " + obj.getString("e_name"));
                                }

                                if (obj.getString("meaning").equals("") || obj.getString("meaning").equals(null)) {
                                    itemModelObj.setMeaning("N/A");
                                } else {
                                    itemModelObj.setMeaning(obj.getString("meaning"));
                                }

                                if (obj.getString("synonym").equals("") || obj.getString("synonym").equals(null)) {
                                    itemModelObj.setSynonym("Synonym: " + "N/A");
                                } else {
                                    itemModelObj.setSynonym("Synonym: " + obj.getString("synonym"));
                                }

                                if (obj.getString("antonym").equals("") || obj.getString("antonym").equals(null)) {
                                    itemModelObj.setAntonym("Antonym: " + "N/A");
                                } else {
                                    itemModelObj.setAntonym("Antonym: " + obj.getString("antonym"));
                                }

                                if (obj.getString("example").equals("") || obj.getString("example").equals(null)) {
                                    itemModelObj.setExample("Example: N/A");
                                } else {
                                    itemModelObj.setExample("Example: " + obj.getString("example"));
                                }
                                if (!Arrays.asList(no_images).contains(sub_menu_name)) {
                                    if (obj.getString("image-url").equals("") || obj.getString("image-url").equals(null)) {
                                        itemModelObj.setImageurl("N/A");
                                    } else {
                                        itemModelObj.setImageurl(obj.getString("image-url").replaceAll("[ ]", "%20"));  //.replaceAll("[ ]", "%20") is so important for the image url which contains space
                                    }
                                }
                                itemList.add(itemModelObj);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        }
                        adapter.notifyDataSetChanged();
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                hidepDialog();
            }
        });
//        This code is for retry for load large JSON
        req.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(req);
    }


    //This array request is for Form of verbs FROM Others category
    private void makeJsonArrayRequestFormOfVerbs(String url_items) {
        showpDialog();
        JsonArrayRequest req = new JsonArrayRequest(url_items,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            jsonResponse = "";
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject obj = (JSONObject) response.get(i);

                                ItemModel itemModelObj = new ItemModel();
                                itemModelObj.setName(obj.getString("meaning"));
                                itemModelObj.setMeaning(obj.getString("present"));
                                itemModelObj.setSynonym("Past: " + obj.getString("past"));
                                itemModelObj.setAntonym("Past participle: " + obj.getString("past_participle"));
                                itemList.add(itemModelObj);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        }
                        adapter.notifyDataSetChanged();
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                hidepDialog();
            }
        });
        //        This code is for retry for load large JSON
        req.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(req);
    }


    //This array request is for PhrasesIdioms & Probverb from Others category
    private void makeJsonArrayRequestPhrasesIdiomsProbverb(String url_items) {
        showpDialog();
        JsonArrayRequest req = new JsonArrayRequest(url_items,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            jsonResponse = "";
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject obj = (JSONObject) response.get(i);

                                ItemModel itemModelObj = new ItemModel();
                                itemModelObj.setName(obj.getString("name"));
                                if (sub_menu_name.equals("Proverb")) {
                                    itemModelObj.setMeaning(obj.getString("translate"));
                                } else {
                                    itemModelObj.setMeaning(obj.getString("e_meaning"));
                                }
                                if (sub_menu_name.equals("Proverb")) {
                                    itemModelObj.setSynonym("উচ্চারণ: " + obj.getString("e_name"));
                                } else {
                                    itemModelObj.setSynonym("Means: " + obj.getString("meaning"));
                                }
                                if (sub_menu_name.equals("Proverb")) {
                                    itemModelObj.setAntonym("");
                                } else {
                                    itemModelObj.setAntonym("Example: " + obj.getString("example"));
                                }
                                itemList.add(itemModelObj);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        }
                        adapter.notifyDataSetChanged();
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                hidepDialog();
            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(req);
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public void noMoreUserToast() {
        Toast toastEmptyUser = Toast.makeText(getApplicationContext(),
                "No words found", Toast.LENGTH_SHORT);
        toastEmptyUser.setGravity(Gravity.CENTER_HORIZONTAL, 0, 0);
        toastEmptyUser.show();
    }

    String no_images[] = {
            "GRE",
            "IELTS Words",
            "Daily words",
            "Magical words",
            "News paper words",
            "Compound words",
            "Tools",
            "Professions And Occupations",
            "Business",
            "Stationery",
            "Ailments And Body Condtions",
            "Dresses",
            "Relations",
            "Speaking preposition",
            "Collected Words",
            "Official words",
            "Group verbs",

            "Vocabulary",
            "Manners and Dialogue",
            "Terminology",
            "Synonym",
            "Antonym"
    };
}
