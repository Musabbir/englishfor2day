package com.musabbir.englishfor2day;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;


/**
 * Created by MUSABBIR MAMUN on 26-Jul-16.
 */

public class SubMenu extends Activity {

    private ListView lv;
    ArrayAdapter<String> adapter;
    String menu_name = "";
    private final static int FADE_DURATION = 1000;
    TextView sub_menu_header;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_menu_items);

        SavedWordsDbOperations swddbObj = new SavedWordsDbOperations(getApplicationContext());
        Bundle extras = getIntent().getExtras();
        menu_name = extras.getString("menu_name");

        sub_menu_header = (TextView) findViewById(R.id.sub_menu_header);
        if (menu_name.equals("child")) {
            sub_menu_header.setText("VOCABULARY");
        } else {
            sub_menu_header.setText(menu_name.toUpperCase());
        }
        lv = (ListView) findViewById(R.id.list);
        if (menu_name.equals("child")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, submenus_child);
        } else if (menu_name.equals("spoken")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, spoken);
        } else if (menu_name.equals("video_lec")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, video_lec);
        } else if (menu_name.equals("grammar")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, grammar);
        } else if (menu_name.equals("gre")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, gre);
        } else if (menu_name.equals("article")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, article);
        } else if (menu_name.equals("others")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, others);
        } else if (menu_name.equals("saved_words")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, swddbObj.getAllSubgaegory());
        } else if (menu_name.equals("more")) {
            adapter = new ArrayAdapter<String>(this, R.layout.sub_menu_list_item, R.id.submenu_name, others);
        }
        lv.setAdapter(adapter);
        //setAnimation(lv);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                TextView submenu_name = (TextView) view.findViewById(R.id.submenu_name);
                String subMenuText = submenu_name.getText().toString();
//Sub menu for child start
                if (subMenuText.equals("Fruits")) {
                    openItemActivity("http://englishfor2day.com/feed/fruits.json", subMenuText);
                } else if (subMenuText.equals("Animals")) {
                    openItemActivity("http://englishfor2day.com/feed/animals.json", subMenuText);
                } else if (subMenuText.equals("GRE")) {
                    openItemActivity("http://englishfor2day.com/feed/gre.json", subMenuText);
                } else if (subMenuText.equals("IELTS Words")) {
                    openItemActivity("http://englishfor2day.com/feed/ielsts.json", subMenuText);
                } else if (subMenuText.equals("Daily words")) {
                    openItemActivity("http://englishfor2day.com/feed/daily-words.json", subMenuText);
                } else if (subMenuText.equals("Vegetables words")) {
                    openItemActivity("http://englishfor2day.com/feed/vegetables-words.json", subMenuText);
                } else if (subMenuText.equals("Birds words")) {
                    openItemActivity("http://englishfor2day.com/feed/birds-words.json", subMenuText);
                } else if (subMenuText.equals("Magical words")) {
                    openItemActivity("http://englishfor2day.com/feed/magical-word.json", subMenuText);
                } else if (subMenuText.equals("News paper words")) {
                    openItemActivity("http://englishfor2day.com/feed/news-paper-words.json", subMenuText);
                } else if (subMenuText.equals("Compound words")) {
                    openItemActivity("http://englishfor2day.com/feed/compound-words.json", subMenuText);
                } else if (subMenuText.equals("Musical Instruments")) {
                    openItemActivity("http://englishfor2day.com/feed/musical-instruments.json", subMenuText);
                } else if (subMenuText.equals("Ornaments and jewels")) {
                    openItemActivity("http://englishfor2day.com/feed/ornaments-and-jewels.json", subMenuText);
                } else if (subMenuText.equals("Tools")) {
                    openItemActivity("http://englishfor2day.com/feed/tools.json", subMenuText);
                } else if (subMenuText.equals("Mountain")) {
                    openItemActivity("http://englishfor2day.com/feed/mountain.json", subMenuText);
                } else if (subMenuText.equals("Parts of Body")) {
                    openItemActivity("http://englishfor2day.com/feed/parts-of-body.json", subMenuText);
                } else if (subMenuText.equals("Professions And Occupations")) {
                    openItemActivity("http://englishfor2day.com/feed/professions-and-occupations.json", subMenuText);
                } else if (subMenuText.equals("Business")) {
                    openItemActivity("http://englishfor2day.com/feed/business.json", subMenuText);
                } else if (subMenuText.equals("Stationery")) {
                    openItemActivity("http://englishfor2day.com/feed/stationery.json", subMenuText);
                } else if (subMenuText.equals("Ailments And Body Condtions")) {
                    openItemActivity("http://englishfor2day.com/feed/ailments-and-body-condtions.json", subMenuText);
                } else if (subMenuText.equals("Dresses")) {
                    openItemActivity("http://englishfor2day.com/feed/dresses.json", subMenuText);
                } else if (subMenuText.equals("Relations")) {
                    openItemActivity("http://englishfor2day.com/feed/relations.json", subMenuText);
                } else if (subMenuText.equals("Cereals And Eatables")) {
                    openItemActivity("http://englishfor2day.com/feed/cereals-and-eatables.json", subMenuText);
                } else if (subMenuText.equals("Speaking preposition")) {
                    openItemActivity("http://englishfor2day.com/feed/speaking-preposition.json", subMenuText);
                } else if (subMenuText.equals("Collected Words")) {
                    openItemActivity("http://englishfor2day.com/feed/collected-word.json", subMenuText);
                } else if (subMenuText.equals("Official words")) {
                    openItemActivity("http://englishfor2day.com/feed/official-vocabulary.json", subMenuText);
                } else if (subMenuText.equals("Group verbs")) {
                    openItemActivity("http://englishfor2day.com/feed/group-verbs.json", subMenuText);
                } else if (subMenuText.equals("Spices")) {
                    openItemActivity("http://englishfor2day.com/feed/spices.json", subMenuText);
                } else if (subMenuText.equals("Physics")) {
                    openItemActivity("http://englishfor2day.com/feed/physics.json", subMenuText);
                } else if (subMenuText.equals("Biology")) {
                    openItemActivity("http://englishfor2day.com/feed/biology.json", subMenuText);
                }
                //Sub menu for child end
                else if (subMenuText.equals("General")) {
                    openItemActivity("http://www.englishfor2day.com/feed/gre-general.json", subMenuText);
                } else if (subMenuText.equals("Barron")) {
                    openItemActivity("http://www.englishfor2day.com/feed/gre-barron.json", subMenuText);
                }
                //Sub menu for GRE end
                else if (subMenuText.equals("Vocabulary")) {
                    openItemActivity("http://www.englishfor2day.com/feed/vocabulary.json", subMenuText);
                } else if (subMenuText.equals("Manners and Dialogue")) {
                    openItemActivity("http://englishfor2day.com/feed/manners-and-dialogue.json", subMenuText);
                } else if (subMenuText.equals("Terminology")) {
                    openItemActivity("http://www.englishfor2day.com/feed/terminology.json", subMenuText);
                } else if (subMenuText.equals("Synonym")) {
                    openItemActivity("http://englishfor2day.com/feed/synonym.json", subMenuText);
                } else if (subMenuText.equals("Antonym")) {
                    openItemActivity("http://www.englishfor2day.com/feed/antonym.json", subMenuText);
                }
                //Submenu for All end

                //Submenu for others start
                else if (subMenuText.equals("Form of verbs")) {
                    openItemActivity("http://www.englishfor2day.com/feed/form-of-verbs.json", subMenuText);
                } else if (subMenuText.equals("Words often confused")) {
                    openItemActivity("http://www.englishfor2day.com/feed/words-often-confused.json", subMenuText);
                } else if (subMenuText.equals("Idioms and Phrase")) {
                    openItemActivity("http://www.englishfor2day.com/feed/idioms-phrase.json", subMenuText);
                } else if (subMenuText.equals("Right form of verbs")) {
                    openItemActivity("http://www.englishfor2day.com/feed/right-form-of-verb.json", subMenuText);
                } else if (subMenuText.equals("Acronym and abbreviation")) {
                    openItemActivity("http://www.englishfor2day.com/feed/acronym-and-abbreviation.json", subMenuText);
                } else if (subMenuText.equals("Proverb")) {
                    openItemActivity("http://www.englishfor2day.com/feed/proverb.json", subMenuText);
                } else if (subMenuText.equals("Prefix")) {
                    openItemActivity("http://www.englishfor2day.com/feed/prefix.json", subMenuText);
                } else if (subMenuText.equals("Basic grammar")) {
                    openItemActivity("http://www.englishfor2day.com/feed/basic-grammar.json", subMenuText);
                } else if (subMenuText.equals("Apostrophe s")) {
                    openItemActivity("http://www.englishfor2day.com/feed/apostrophe-s.json", subMenuText);
                }
                //Submenu for others end

                //Submenu for Article start
                else if (subMenuText.equals("Essay")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/essay.json", subMenuText);
                } else if (subMenuText.equals("Paragraph")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/paragraph.json", subMenuText);
                } else if (subMenuText.equals("Dialogue")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/dialogue.json", subMenuText);
                } else if (subMenuText.equals("Story writing")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/story-writing.json", subMenuText);
                } else if (subMenuText.equals("Letter")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/general-letter.json", subMenuText);
                } else if (subMenuText.equals("Cover letter")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/cover-letter.json", subMenuText);
                } else if (subMenuText.equals("Certification")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/certification.json", subMenuText);
                } else if (subMenuText.equals("Application")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/general-application.json", subMenuText);
                } else if (subMenuText.equals("Job application")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/job-application.json", subMenuText);
                } else if (subMenuText.equals("Official application")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/official-application.json", subMenuText);
                }
                //Submenu for Article end

                //Submenu for video lecture start
                else if (subMenuText.equals("Video Lecture")) {
                    subSubMenuActivity("http://www.englishfor2day.com/feed/spokenvideo.json", subMenuText);
                }
                //Submenu for video lecture end

            }
        });
    }


    private void setAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }

    public void openItemActivity(String sub_menu_url, String sub_menu_name) {
        Intent i;
        if (menu_name.equals("saved_words")) {
            i = new Intent(SubMenu.this, SavedWords.class);
        } else {
            i = new Intent(SubMenu.this, Items.class);
        }
        i.putExtra("sub_menu_url", sub_menu_url);
        i.putExtra("sub_menu_name", sub_menu_name);
        startActivity(i);
    }


    public void subSubMenuActivity(String sub_menu_url, String sub_menu_name) {
        Intent i;
        if (menu_name.equals("video_lec")) {
            i = new Intent(SubMenu.this, Videos.class);
        } else {
            i = new Intent(SubMenu.this, SubSubMenu.class);
        }
        i.putExtra("sub_menu_url", sub_menu_url);
        i.putExtra("sub_menu_name", sub_menu_name);
        startActivity(i);
    }

    public String checkNetConnection() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            return "netEnable";
        } else {
            return "netDisable";
        }
    }

    String submenus_child[] = {
            "Fruits",
            "Animals",
            "GRE",
            "IELTS Words",
            "Daily words",
            "Vegetables words",
            "Birds words",
            "Magical words",
            "News paper words",
            "Compound words",
            "Musical Instruments",
            "Ornaments and jewels",
            "Tools",
            "Mountain",
            "Parts of Body",
            "Professions And Occupations",
            "Business",
            "Stationery",
            "Ailments And Body Condtions",
            "Dresses",
            "Relations",
            "Cereals And Eatables",
            "Speaking preposition",
            "Collected Words",
            "Official words",
            "Group verbs",
            "Spices",
            "Physics",
            "Biology"};
    String spoken[] = {
            "Spoken"
    };
    String video_lec[] = {
            "Video Lecture"
    };
    String grammar[] = {
            "Grammar"
    };
    String gre[] = {
            "General",
            "Barron"
    };
    String article[] = {
            "Essay",
            "Paragraph",
            "Dialogue",
            "Story writing",
            "Letter",
            "Cover letter",
            "Certification",
            "Application",
            "Job application",
            "Official application",
    };

    String others[] = {
            "Form of verbs",
            "Words often confused",
            "Idioms and Phrase",
            "Right form of verbs",
            "Acronym and abbreviation",
            "Proverb",
            "Prefix",
            "Basic grammar",
            "Apostrophe s",
            "Synonym",
            "Antonym",
            "Terminology"
    };
}