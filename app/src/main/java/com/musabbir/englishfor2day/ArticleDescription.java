package com.musabbir.englishfor2day;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.sdsmdg.tastytoast.TastyToast;

import java.util.HashMap;

/**
 * Created by Musabbir on 19-Sep-16.
 */

//If you want Material design Title bar then  you should extends AppCompatActivity
public class ArticleDescription extends AppCompatActivity implements View.OnClickListener {

    private Boolean isFabOpen = false;
    private FloatingActionButton fab, fabCopy, fabShare, fabSpeak, fabYoutube;
    private Animation fab_open, fab_close, rotate_forward, rotate_backward;
    TextView tvDetails, tvCaption;
    String description, caption, sub_menu_name, youtubeLink;
    TextToSpeech tts;
    boolean isSpeak = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.article_description);

        Bundle extras = getIntent().getExtras();
        sub_menu_name = extras.getString("sub_menu_name");
        caption = extras.getString("caption");
        description = extras.getString("description");
        description = description.replace("?", "?\n\n");

        if (sub_menu_name.equals("Spoken")) {
            youtubeLink = extras.getString("youtubeLink");
            description = description.replace(".", ".\n\n");
        }
        //description = "Amar sonar bangla. Ami tomay valobasi";

        tvCaption = (TextView) findViewById(R.id.tvCaption);
        tvDetails = (TextView) findViewById(R.id.tvDetails);

        //setTitle(Html.fromHtml("<small>"+"ASD"+"</small>"));
        setTitle(sub_menu_name);
        tvCaption.setText(caption);
        tvDetails.setText(description);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabCopy = (FloatingActionButton) findViewById(R.id.fabCopy);
        fabShare = (FloatingActionButton) findViewById(R.id.fabShare);
        fabSpeak = (FloatingActionButton) findViewById(R.id.fabSpeak);
        fabYoutube = (FloatingActionButton) findViewById(R.id.fabYoutube);

        fab_open = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fabCopy.setOnClickListener(this);
        fabShare.setOnClickListener(this);
        fabSpeak.setOnClickListener(this);
        fabYoutube.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        String finalStr = caption + "\n" + description + "\nvia Englishfor2day\nGet the app @\nhttp://www.englishfor2day.com/";

        switch (id) {
            case R.id.fab:
                animateFAB();
                break;
            case R.id.fabCopy:
                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                    clipboard.setText(finalStr);
                } else {
                    android.content.ClipboardManager clipboard = (android.content.ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    android.content.ClipData clip = android.content.ClipData.newPlainText("Explored name", finalStr);
                    clipboard.setPrimaryClip(clip);
                }
                TastyToast.makeText(getApplicationContext(), "All text copied to clipboard", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
                break;
            case R.id.fabShare:
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Englishfor2day");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, finalStr);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;

            case R.id.fabSpeak:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                    TastyToast.makeText(getApplicationContext(), "This feature isn't available for your device", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        if (isSpeak) {
                            ttsGreater21(description);
                            fabSpeak.setImageDrawable(getResources().getDrawable(R.mipmap.ic_stopp, getBaseContext().getTheme()));
                            isTTSSpeaking();
                            isSpeak = false;
                        } else if (!isSpeak) {
                            fabSpeak.setImageDrawable(getResources().getDrawable(R.mipmap.ic_volume, getBaseContext().getTheme()));
                            tts.shutdown();
                            isSpeak = true;
                        }
                    } else {
                        if (isSpeak) {
                            ttsUnder20(description);
                            fabSpeak.setImageDrawable(getResources().getDrawable(R.mipmap.ic_stopp));
                            isTTSSpeaking();
                            isSpeak = false;
                        } else if (!isSpeak) {
                            fabSpeak.setImageDrawable(getResources().getDrawable(R.mipmap.ic_volume));
                            tts.shutdown();
                            isSpeak = true;
                        }
                    }
                }
                break;

            case R.id.fabYoutube:
                if (youtubeLink.length() > 5) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeLink)));
                } else {
                    TastyToast.makeText(getApplicationContext(), "No youtube link found", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                }
                break;
        }
    }

    public void animateFAB() {
        if (isFabOpen) {
            if (sub_menu_name.equals("Spoken")) {
                fabYoutube.startAnimation(fab_close);
                fabYoutube.setClickable(false);
            }
            fab.startAnimation(rotate_backward);
            fabCopy.startAnimation(fab_close);
            fabShare.startAnimation(fab_close);
            fabSpeak.startAnimation(fab_close);

            fabCopy.setClickable(false);
            fabShare.setClickable(false);
            fabSpeak.setClickable(false);
            isFabOpen = false;
        } else {
            if (sub_menu_name.equals("Spoken")) {
                fabYoutube.startAnimation(fab_open);
                fabYoutube.setClickable(true);
            }
            fab.startAnimation(rotate_forward);
            fabCopy.startAnimation(fab_open);
            fabShare.startAnimation(fab_open);
            fabSpeak.startAnimation(fab_open);

            fabCopy.setClickable(true);
            fabShare.setClickable(true);
            fabSpeak.setClickable(true);
            isFabOpen = true;
        }
    }


    //    Code for text to speech with checking different android build version
    @SuppressWarnings("deprecation")
    public void ttsUnder20(final String text) {
        final HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts = new TextToSpeech(getBaseContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, map);
                fabSpeak.setImageDrawable(getResources().getDrawable(R.mipmap.ic_volume));
                tts.shutdown();
                isSpeak = true;
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void ttsGreater21(final String text) {
        final String utteranceId = this.hashCode() + "";
        tts = new TextToSpeech(getBaseContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
            }
        });
    }


    public void isTTSSpeaking() {
        final Handler h = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                if (!tts.isSpeaking()) {
                    //onTTSSpeechFinished();
                    fabSpeak.setImageDrawable(getResources().getDrawable(R.mipmap.ic_volume, getBaseContext().getTheme()));
                    tts.shutdown();
                    isSpeak = true;
                    return;
                }
                h.postDelayed(this, 1000);
            }
        };
        h.postDelayed(r, 1000);
    }

    @Override
    public void onBackPressed() {
        try {
            if (tts.isSpeaking()) {
                tts.shutdown();
            }
            this.finish();
        } catch (Exception ex) {
            this.finish();
        }
    }
}
