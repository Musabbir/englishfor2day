package com.musabbir.englishfor2day;

/**
 * Created by Musabbir on 30-Aug-16.
 */
public class SavedWordsTable {
    public static final String TABLE = "saved_words";

    public static final String KEY_id = "id";
    public static final String KEY_subcategory = "subcategory";
    public static final String KEY_name = "name";
    public static final String KEY_meaning = "meaning";
    public static final String KEY_img = "image";
    public static final String KEY_text1 = "text1";
    public static final String KEY_text2 = "text2";
    public static final String KEY_text3 = "text3";


    public int id;
    public String subcategory;
    public String name;
    public String meaning;
    public String image;
    public String text1;
    public String text2;
    public String text3;
}