package com.musabbir.englishfor2day;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.musabbir.englishfor2day.adapter.SavedWordsAdapter;
import com.musabbir.englishfor2day.model.SavedWordsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MUSABBIR MAMUN on 01-Sep-16.
 */
public class SavedWords extends Activity {

    SavedWordsAdapter swAdapter;
    ArrayList<SavedWordsModel> savedWordsArray = new ArrayList<SavedWordsModel>();
    public SavedWordsDbOperations repo;
    String sub_menu_name = "";
    ImageView search_img;
    TextView itemsHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.items);

        Bundle extras = getIntent().getExtras();
        sub_menu_name = extras.getString("sub_menu_name");

        search_img = (ImageView) findViewById(R.id.search_img);
        itemsHeader = (TextView) findViewById(R.id.itemsHeader);
        search_img.setVisibility(View.GONE);
        itemsHeader.setText(sub_menu_name);

        repo = new SavedWordsDbOperations(this);

        List<SavedWordsModel> words = repo.getAllSavedWords(sub_menu_name);
        for (SavedWordsModel cn : words) {
            savedWordsArray.add(cn);
        }
        swAdapter = new SavedWordsAdapter(SavedWords.this, R.layout.items_saved_items, savedWordsArray);
        ListView dataList = (ListView) findViewById(R.id.listView);
        dataList.setAdapter(swAdapter);
    }
}
