package com.musabbir.englishfor2day.model;

/**
 * Created by MUSABBIR MAMUN on 01-Sep-16.
 */
public class SavedWordsModel {

    public SavedWordsModel() {

    }

    public SavedWordsModel(String name, String meaning, String image, String text1, String text2, String text3) {
        super();
        this.name = name;
        this.meaning = meaning;
        this.image = image;
        this.text1 = text1;
        this.text2 = text2;
        this.text3 = text3;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText3() {
        return text3;
    }

    public void setText3(String text3) {
        this.text3 = text3;
    }

    public String getText2() {
        return text2;
    }

    public void setText2(String text2) {
        this.text2 = text2;
    }

    public String getText1() {
        return text1;
    }

    public void setText1(String text1) {
        this.text1 = text1;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    private String name;
    private String meaning;
    private String image;
    private String text1;
    private String text2;
    private String text3;

}
