package com.musabbir.englishfor2day.model;

/**
 * Created by Musabbir on 19-Sep-16.
 */
public class SubSubMenuModel {
    private String id;
    private String article_cat;
    private String caption;
    private String description;

    public String getArticle_cat() {
        return article_cat;
    }

    public void setArticle_cat(String article_cat) {
        this.article_cat = article_cat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
