package com.musabbir.englishfor2day.model;

/**
 * Created by MUSABBIR MAMUN on 23-Sep-16.
 */
public class VideosModel {
    private String vidCaption;
    private String idVid;
    private String video_type;
    private String idimgurl;
    private String videoUrl;

    public VideosModel() {

    }

    public VideosModel(String vidCaption, String idVid, String video_type, String idimgurl, String videoUrl) {
        this.vidCaption = vidCaption;
        this.idVid = idVid;
        this.video_type = video_type;
        this.idimgurl = idimgurl;
        this.videoUrl = videoUrl;
    }

    public String getVidCaption() {
        return vidCaption;
    }

    public void setVidCaption(String vidCaption) {
        this.vidCaption = vidCaption;
    }

    public String getIdVid() {
        return idVid;
    }

    public void setIdVid(String idVid) {
        this.idVid = idVid;
    }

    public String getVideo_type() {
        return video_type;
    }

    public void setVideo_type(String video_type) {
        this.video_type = video_type;
    }

    public String getIdimgurl() {
        return idimgurl;
    }

    public void setIdimgurl(String idimgurl) {
        this.idimgurl = idimgurl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
