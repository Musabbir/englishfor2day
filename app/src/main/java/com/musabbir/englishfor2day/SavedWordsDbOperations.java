package com.musabbir.englishfor2day;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.musabbir.englishfor2day.model.SavedWordsModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MUSABBIR MAMUN on 30-Aug-16.
 */
public class SavedWordsDbOperations {

    public DBHelper dbHelper;
    SQLiteDatabase db;

    public SavedWordsDbOperations(Context context) {
        dbHelper = new DBHelper(context);
    }

    public String insert(SavedWordsTable item) {
        if (checkWordDuplicacy(item.meaning).equals(item.meaning)) {
            return "duplicate";
        } else {
            try {
                db = dbHelper.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(SavedWordsTable.KEY_subcategory, item.subcategory);
                values.put(SavedWordsTable.KEY_name, item.name);
                values.put(SavedWordsTable.KEY_meaning, item.meaning);
                values.put(SavedWordsTable.KEY_img, item.image);
                values.put(SavedWordsTable.KEY_text1, item.text1);
                values.put(SavedWordsTable.KEY_text2, item.text2);
                values.put(SavedWordsTable.KEY_text3, item.text3);

                db.insert(SavedWordsTable.TABLE, null, values);
                db.close(); // Closing database connection
                return "inserted";
            } catch (Exception ex) {
                return "failed";
            }
        }
    }

    public List<String> getAllSubgaegory() {
        db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT DISTINCT " + SavedWordsTable.KEY_subcategory + " FROM " + SavedWordsTable.TABLE;
        List<String> subcatList = new ArrayList<String>();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                subcatList.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return subcatList;
    }

    public String checkWordDuplicacy(String checkWord) {
        db = dbHelper.getReadableDatabase();
        String word = "";
        String word_search_query = "SELECT " + SavedWordsTable.KEY_meaning + " FROM " + SavedWordsTable.TABLE + " WHERE " + SavedWordsTable.KEY_meaning + " = " + "'" + checkWord + "'";
        Cursor cursor = db.rawQuery(word_search_query, null);
        while (cursor.moveToNext()) {
            word = cursor.getString(0);
        }
        db.close();
        return word;
    }

    public List<SavedWordsModel> getAllSavedWords(String sub_menu_name) {
        List<SavedWordsModel> savedWordsList = new ArrayList<SavedWordsModel>();
        String selectQuery = "SELECT  " +
                SavedWordsTable.KEY_name + "," +
                SavedWordsTable.KEY_meaning + "," +
                SavedWordsTable.KEY_img + "," +
                SavedWordsTable.KEY_text1 + "," +
                SavedWordsTable.KEY_text2 + "," +
                SavedWordsTable.KEY_text3 +
                " FROM " + SavedWordsTable.TABLE + " WHERE " + SavedWordsTable.KEY_subcategory + " = " + "'" + sub_menu_name + "'";
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                SavedWordsModel swModel = new SavedWordsModel();
                swModel.setName(cursor.getString(0));
                swModel.setMeaning(cursor.getString(1));
                swModel.setImage(cursor.getString(2));
                swModel.setText1(cursor.getString(3));
                swModel.setText2(cursor.getString(4));
                swModel.setText3(cursor.getString(5));
                savedWordsList.add(swModel);
            } while (cursor.moveToNext());
        }
        db.close();
        return savedWordsList;
    }

    public void deleteSavedWord(String meaning){
        db = dbHelper.getWritableDatabase();
        // It's a good practice to use parameter ?, instead of concatenate string
        db.delete(SavedWordsTable.TABLE, SavedWordsTable.KEY_meaning+ "= ?", new String[]{meaning});
        db.close();
    }
}
