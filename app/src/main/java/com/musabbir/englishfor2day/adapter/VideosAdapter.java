package com.musabbir.englishfor2day.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.musabbir.englishfor2day.R;
import com.musabbir.englishfor2day.model.VideosModel;

import java.util.List;

/**
 * Created by MUSABBIR MAMUN on 23-Sep-16.
 */
public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.MyViewHolder> {

    private Context mContext;
    private List<VideosModel> vidList;

    String vidCaptionStr, idVidStr, video_typeStr, idimgurlStr, videoUrlStr;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView vidCaption, idVid, video_type, idimgurl, videoUrl;
        public ImageView videoImg;

        public MyViewHolder(View view) {
            super(view);
            vidCaption = (TextView) view.findViewById(R.id.vidCaption);
            idVid = (TextView) view.findViewById(R.id.idVid);
            video_type = (TextView) view.findViewById(R.id.video_type);
            videoUrl = (TextView) view.findViewById(R.id.videoUrl);

            idimgurl = (TextView) view.findViewById(R.id.idimgurl);
            videoImg = (ImageView) view.findViewById(R.id.videoImg);

            vidCaptionStr = vidCaption.getText().toString();
            idVidStr = idVid.getText().toString();
            video_typeStr = video_type.getText().toString();
            videoUrlStr = videoUrl.getText().toString();
            idimgurlStr = idimgurl.getText().toString();
        }
    }

    public VideosAdapter(Context mContext, List<VideosModel> vidList) {
        this.mContext = mContext;
        this.vidList = vidList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.videos_cardview, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        VideosModel videos = vidList.get(position);
        holder.vidCaption.setText(videos.getVidCaption());
        holder.idVid.setText(videos.getIdVid());
        holder.video_type.setText(videos.getVideo_type());
        holder.idimgurl.setText(videos.getIdimgurl());
        holder.videoUrl.setText(videos.getVideoUrl());

        // loading album cover using Glide library
        Glide.with(mContext).load(videos.getIdimgurl()).placeholder(R.mipmap.ic_youtube_red).error(R.mipmap.ic_youtube_red).into(holder.videoImg);

        holder.videoImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoUrlStr = holder.videoUrl.getText().toString();
                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(videoUrlStr)));
            }
        });

        setAnimation(holder.itemView);
    }

    @Override
    public int getItemCount() {
        return vidList.size();
    }


    private void setAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(1000);
        view.startAnimation(anim);
    }
}
