package com.musabbir.englishfor2day.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.musabbir.englishfor2day.R;
import com.musabbir.englishfor2day.SubSubMenu;
import com.musabbir.englishfor2day.model.ItemModel;
import com.musabbir.englishfor2day.model.SubSubMenuModel;
import java.util.List;

/**
 * Created by Musabbir on 19-Sep-16.
 */
public class SubSubMenuAdapter extends BaseAdapter{

    private SubSubMenu ssmObj;
    private LayoutInflater inflater;
    private List<SubSubMenuModel> subItems;
    SubSubMenuModel m;
    Context context;
    private final static int FADE_DURATION = 1000;


    //The last parameter context is so important here to get the Application context which is sent from SubSubMenu class
    public SubSubMenuAdapter(SubSubMenu ssmObj, List<SubSubMenuModel> subItems, Context context) {
        this.ssmObj = ssmObj;
        this.subItems = subItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return subItems.size();
    }

    @Override
    public Object getItem(int location) {
        return subItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) ssmObj
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.sub_sub_menu_list_item, null);


        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView article_cat = (TextView) convertView.findViewById(R.id.article_cat);
        TextView caption = (TextView) convertView.findViewById(R.id.caption);
        TextView description = (TextView) convertView.findViewById(R.id.description);

        final SubSubMenuModel m = subItems.get(position);

        id.setText(m.getId());
        article_cat.setText(m.getArticle_cat());
        caption.setText(m.getCaption());
        description.setText(m.getDescription());
        //setAnimation(convertView);
        return convertView;
    }

    private void setAnimation(View view) {
        ScaleAnimation anim = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setDuration(FADE_DURATION);
        view.startAnimation(anim);
    }
}