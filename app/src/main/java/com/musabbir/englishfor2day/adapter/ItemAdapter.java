package com.musabbir.englishfor2day.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.musabbir.englishfor2day.AppController;
import com.musabbir.englishfor2day.Items;
import com.musabbir.englishfor2day.R;
import com.musabbir.englishfor2day.SavedWordsDbOperations;
import com.musabbir.englishfor2day.SavedWordsTable;
import com.musabbir.englishfor2day.model.ItemModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by MUSABBIR MAMUN on 26-Jul-16.
 */

public class ItemAdapter extends BaseAdapter implements Filterable {

    private Items itemObj;
    private LayoutInflater inflater;
    private List<ItemModel> wordItems,wordItems2;
    ImageLoader imageLoader = AppController.getInstance().getImageLoader();
    private ValueFilter valueFilter;
    ItemModel m;
    TextToSpeech tts;
    String speakStr = "";
    Context context;
    SavedWordsDbOperations swdbObj;

    //The last parameter context is so important here to get the Application context which is sent from Items class
    public ItemAdapter(Items itemObj, List<ItemModel> wordItems, Context context) {
        this.itemObj = itemObj;
        this.wordItems = wordItems;
        this.wordItems2 = wordItems;
        this.context = context;
        swdbObj = new SavedWordsDbOperations(context);
    }

    @Override
    public int getCount() {
        return wordItems.size();
    }

    @Override
    public Object getItem(int location) {
        return wordItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) itemObj
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.items_list_items, null);

        if (imageLoader == null)
            imageLoader = AppController.getInstance().getImageLoader();
        NetworkImageView thumbNail = (NetworkImageView) convertView
                .findViewById(R.id.imageurl);

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView e_name = (TextView) convertView.findViewById(R.id.e_name);
        TextView meaning = (TextView) convertView.findViewById(R.id.meaning);
        TextView synonym = (TextView) convertView.findViewById(R.id.synonym);
        TextView antonym = (TextView) convertView.findViewById(R.id.antonym);
        TextView example = (TextView) convertView.findViewById(R.id.example);

        ImageButton speak = (ImageButton) convertView.findViewById(R.id.speak);
        ImageButton save = (ImageButton) convertView.findViewById(R.id.save);

//        speakStr = meaning.getText().toString();

        final ItemModel m = wordItems.get(position); //After clicking the speak button & get the value meaning the object m need to be declare as final
        thumbNail.setImageUrl(m.getImageurl(), imageLoader);

        name.setText(m.getName());
        e_name.setText(m.getE_name());
        meaning.setText(m.getMeaning());
        synonym.setText(m.getSynonym());
        antonym.setText(m.getAntonym());
        example.setText(m.getExample());

        speak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speakStr = m.getMeaning().toString();
                Toast toast = Toast.makeText(itemObj.getBaseContext(), speakStr, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ttsGreater21(speakStr);
                } else {
                    ttsUnder20(speakStr);
                }
//                tts = new TextToSpeech(itemObj.getBaseContext(), new TextToSpeech.OnInitListener() {
//                    @Override
//                    public void onInit(int status) {
//                        tts.speak(speakStr, TextToSpeech.QUEUE_FLUSH, null);
//                    }
//                });
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SavedWordsTable item = new SavedWordsTable();
                item.subcategory = itemObj.sub_menu_name;
                item.name = filterCharacter(m.getName());
                item.meaning = filterCharacter(m.getMeaning());
                item.image = m.getImageurl();
                item.text1 = filterCharacter(m.getSynonym());
                item.text2 = filterCharacter(m.getAntonym());
                item.text3 = filterCharacter(m.getExample());

                if (swdbObj.insert(item).equals("inserted")) {
                    showToast("Word saved to your collection");
                } else if (swdbObj.insert(item).equals("duplicate")) {
                    showToast("You already saved this word");
                } else if (swdbObj.insert(item).equals("failed")) {
                    showToast("Failed to save this word");
                }
            }
        });
        return convertView;
    }

    public String filterCharacter(String str) {
//        String filteredString = str.replaceAll("[<>\\[\\],*+.%&@$#?(){}'/-]", "");
        String filteredString = str.replaceAll("'", "");
        return filteredString;
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(itemObj.getBaseContext(), message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {

            valueFilter = new ValueFilter();
        }

        return valueFilter;
    }

    private class ValueFilter extends Filter {

        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            constraint = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<ItemModel> filterList = new ArrayList<ItemModel>();
                for (int i = 0; i < wordItems2.size(); i++) {
                    ItemModel items = new ItemModel();
                    if ((wordItems2.get(i).getMeaning().toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        items.setName(wordItems2.get(i).getName());
                        items.setE_name(wordItems2.get(i).getE_name());
                        items.setMeaning(wordItems2.get(i).getMeaning());
                        items.setSynonym(wordItems2.get(i).getSynonym());
                        items.setAntonym(wordItems2.get(i).getAntonym());
                        items.setExample(wordItems2.get(i).getExample());
                        items.setImageurl(wordItems2.get(i).getImageurl());
                        filterList.add(items);
                    }
                }
                results.count = filterList.size();
                results.values = filterList;

            } else if (constraint.equals("")) {
                results.count = wordItems.size();
                results.values = wordItems;
            }
            return results;
        }

        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            wordItems = (ArrayList<ItemModel>) results.values;
            notifyDataSetChanged();
        }
    }


    //    Code for text to speech with checking different android build version
    @SuppressWarnings("deprecation")
    public void ttsUnder20(final String text) {
        final HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts = new TextToSpeech(itemObj.getBaseContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, map);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void ttsGreater21(final String text) {
        final String utteranceId = this.hashCode() + "";
        tts = new TextToSpeech(itemObj.getBaseContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
            }
        });
    }
}
