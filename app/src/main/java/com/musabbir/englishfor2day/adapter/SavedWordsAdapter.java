package com.musabbir.englishfor2day.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.musabbir.englishfor2day.R;
import com.musabbir.englishfor2day.SavedWords;
import com.musabbir.englishfor2day.model.SavedWordsModel;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by MUSABBIR MAMUN on 01-Sep-16.
 */
public class SavedWordsAdapter extends ArrayAdapter<SavedWordsModel> {

    Context context;
    int layoutResourceId;
    SavedWords swObj;
    TextToSpeech tts;
    String speakStr = "", deleteStr = "";

    ArrayList<SavedWordsModel> data = new ArrayList<SavedWordsModel>();

    public SavedWordsAdapter(SavedWords swObj, int layoutResourceId, ArrayList<SavedWordsModel> data) {
        super(swObj, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.swObj = swObj;
        this.data = data;
    }


    @Override
    public View getView(final int index, final View convertView, ViewGroup parent) {
        View row = convertView;
        savedWordsHolder holder = null;
        final SavedWordsModel s_words = data.get(index);

        if (row == null) {
            LayoutInflater inflater = ((SavedWords) swObj).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new savedWordsHolder();

            holder.image = (ImageView) row.findViewById(R.id.imageurl);
            holder.name = (TextView) row.findViewById(R.id.name);
            holder.meaning = (TextView) row.findViewById(R.id.meaning);
            holder.text1 = (TextView) row.findViewById(R.id.synonym);
            holder.text2 = (TextView) row.findViewById(R.id.antonym);
            holder.text3 = (TextView) row.findViewById(R.id.example);

            holder.speakBtn = (ImageButton) row.findViewById(R.id.speak);
            holder.deleteBtn = (ImageButton) row.findViewById(R.id.delete);
            row.setTag(holder);
        } else {
            holder = (savedWordsHolder) row.getTag();
        }

        String imagePath = s_words.getImage();
        Glide.with(row.getContext()).load(imagePath).into(holder.image);
        holder.name.setText(s_words.getName());
        holder.meaning.setText(s_words.getMeaning());
        holder.text1.setText(s_words.getText1());
        holder.text2.setText(s_words.getText2());
        holder.text3.setText(s_words.getText3());

        final savedWordsHolder finalHolder = holder; //needed to get view after speakBtn click

        holder.speakBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Commited code is tried for ripple effect in this Image Button
                //finalHolder.speakBtn.setBackgroundResource(R.attr.selectableItemBackgroundBorderless);
//                int[] attrs = new int[]{R.attr.selectableItemBackground};
//                TypedArray typedArray = swObj.getBaseContext().obtainStyledAttributes(attrs);
//                int backgroundResource = typedArray.getResourceId(0, 0);
//                view.setBackgroundResource(backgroundResource);
//                typedArray.recycle();

                speakStr = finalHolder.meaning.getText().toString();
                Toast toast = Toast.makeText(swObj.getBaseContext(), speakStr, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    ttsGreater21(speakStr);
                } else {
                    ttsUnder20(speakStr);
                }
            }
        });

        holder.deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteStr = finalHolder.meaning.getText().toString();
                swObj.repo.deleteSavedWord(deleteStr);
                data.remove(index);
                notifyDataSetChanged();
                Toast.makeText(swObj.getBaseContext(), deleteStr + " removed from your collected words", Toast.LENGTH_SHORT).show();
            }
        });

        return row;
    }

    public static class savedWordsHolder {
        ImageView image;
        TextView name;
        TextView meaning;
        TextView text1;
        TextView text2;
        TextView text3;

        ImageButton speakBtn;
        ImageButton deleteBtn;
    }


    //    Code for text to speech with checking different android build version
    @SuppressWarnings("deprecation")
    public void ttsUnder20(final String text) {
        final HashMap<String, String> map = new HashMap<>();
        map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
        tts = new TextToSpeech(swObj.getBaseContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, map);
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void ttsGreater21(final String text) {
        final String utteranceId = this.hashCode() + "";
        tts = new TextToSpeech(swObj.getBaseContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
            }
        });
    }
}
