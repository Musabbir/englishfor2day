package com.musabbir.englishfor2day.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.musabbir.englishfor2day.R;
import com.musabbir.englishfor2day.Spoken;
import com.musabbir.englishfor2day.model.SpokenModel;

import java.util.List;

/**
 * Created by MUSABBIR MAMUN on 19-Sep-16.
 */
public class SpokenAdapter extends BaseAdapter {

    private Spoken spknObj;
    private LayoutInflater inflater;
    private List<SpokenModel> spknItems;
    SpokenModel m;
    Context context;

    //The last parameter context is so important here to get the Application context which is sent from Spoken class
    public SpokenAdapter(Spoken spknObj, List<SpokenModel> spknItems, Context context) {
        this.spknObj = spknObj;
        this.spknItems = spknItems;
        this.context = context;
    }

    @Override
    public int getCount() {
        return spknItems.size();
    }

    @Override
    public Object getItem(int location) {
        return spknItems.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) spknObj.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.spoken_items, null);

        TextView id = (TextView) convertView.findViewById(R.id.id);
        TextView article_type = (TextView) convertView.findViewById(R.id.article_type);
        TextView caption = (TextView) convertView.findViewById(R.id.caption);
        TextView chapter_no = (TextView) convertView.findViewById(R.id.chapter_no);
        TextView chapter_title = (TextView) convertView.findViewById(R.id.chapter_title);
        TextView app_field = (TextView) convertView.findViewById(R.id.app_field);
        TextView dtl = (TextView) convertView.findViewById(R.id.dtl);
        TextView hit = (TextView) convertView.findViewById(R.id.hit);
        TextView youtube = (TextView) convertView.findViewById(R.id.youtube);

        final SpokenModel m = spknItems.get(position);

        id.setText(m.getId());
        article_type.setText(m.getArticle_type());
        caption.setText(m.getCaption());
        chapter_no.setText(m.getChapter_no());
        chapter_title.setText(m.getChapter_title());
        app_field.setText(m.getApp_field());
        dtl.setText(m.getDtl());
        hit.setText(m.getHit());
        youtube.setText(m.getYoutube());
        return convertView;
    }
}