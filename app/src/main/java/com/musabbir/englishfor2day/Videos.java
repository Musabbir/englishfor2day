package com.musabbir.englishfor2day;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.musabbir.englishfor2day.adapter.VideosAdapter;
import com.musabbir.englishfor2day.model.ItemModel;
import com.musabbir.englishfor2day.model.VideosModel;
import com.sdsmdg.tastytoast.TastyToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by MUSABBIR MAMUN on 23-Sep-16.
 */
public class Videos extends AppCompatActivity {
    private RecyclerView recyclerView;
    private VideosAdapter adapter;
    private List<VideosModel> videoList;
    final Context context = this;
    private int MY_SOCKET_TIMEOUT_MS = 15000;
    private ProgressDialog pDialog;
    String jsonResponse = "";
    String sub_menu_name, sub_menu_url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.videos_activity);

        Bundle extras = getIntent().getExtras();
        sub_menu_name = extras.getString("sub_menu_name");
        sub_menu_url = extras.getString("sub_menu_url");
        setTitle(sub_menu_name);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        videoList = new ArrayList<>();
        adapter = new VideosAdapter(this, videoList);

        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");

        makeJsonArrayRequestFormOfVerbs(sub_menu_url);
    }

    private void makeJsonArrayRequestFormOfVerbs(String url_items) {
        showpDialog();
        JsonArrayRequest req = new JsonArrayRequest(url_items,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            jsonResponse = "";
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject obj = (JSONObject) response.get(i);

                                VideosModel videosModelObj = new VideosModel();
                                videosModelObj.setIdVid(obj.getString("id"));
                                videosModelObj.setVideo_type(obj.getString("video_type"));
                                videosModelObj.setVidCaption(obj.getString("caption"));
                                videosModelObj.setVideoUrl(obj.getString("youtube"));
                                videosModelObj.setIdimgurl(videoIdExtractor(obj.getString("youtube")));
                                videoList.add(videosModelObj);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        }
                        adapter.notifyDataSetChanged();
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                hidepDialog();
            }
        });
        //        This code is for retry for load large JSON
        req.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(req);
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    public String videoIdExtractor(String videooUrl) {
        String reversed = new StringBuilder(videooUrl).reverse().toString();
        String[] parts = reversed.split("/");
        String parts1 = parts[0];
        String reverseAgain = new StringBuilder(parts1).reverse().toString();
        String generateThumb = "https://img.youtube.com/vi/" + reverseAgain + "/0.jpg";
        return generateThumb;
    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
}