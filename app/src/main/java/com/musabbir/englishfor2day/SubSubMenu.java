package com.musabbir.englishfor2day;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.musabbir.englishfor2day.adapter.ItemAdapter;
import com.musabbir.englishfor2day.adapter.SubSubMenuAdapter;
import com.musabbir.englishfor2day.model.ItemModel;
import com.musabbir.englishfor2day.model.SubSubMenuModel;
import com.sdsmdg.tastytoast.TastyToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by MUSABBIR MAMUN on 18-Sep-16.
 */
public class SubSubMenu extends Activity {

    private static final int MY_SOCKET_TIMEOUT_MS = 15000;
    private List<SubSubMenuModel> subList = new ArrayList<SubSubMenuModel>();
    public ListView listView;
    private SubSubMenuAdapter adapter;
    private ProgressDialog pDialog;
    String jsonResponse = "";
    public static String url_items = "", sub_menu_name = "";
    String strDesc = "", strCap = "";
    TextView itemHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sub_sub_menu);

        Bundle extras = getIntent().getExtras();
        url_items = extras.getString("sub_menu_url");
        sub_menu_name = extras.getString("sub_menu_name");

        itemHeader = (TextView) findViewById(R.id.itemHeader);
        itemHeader.setText(sub_menu_name);

        listView = (ListView) findViewById(R.id.listView);

//The last parameter getBaseContext() is so important here to send the Application context to SubSubMenuAdapter class
        adapter = new SubSubMenuAdapter(this, subList, getBaseContext());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView artName = (TextView) view.findViewById(R.id.caption);
                strCap = artName.getText().toString();

                TextView desc = (TextView) view.findViewById(R.id.description);
                strDesc = desc.getText().toString();

                Intent intent = new Intent(SubSubMenu.this, ArticleDescription.class);
                intent.putExtra("sub_menu_name", sub_menu_name);
                intent.putExtra("caption", strCap);
                intent.putExtra("description", strDesc);
                startActivity(intent);
            }
        });

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        makeJsonArrayRequest(url_items);
    }


    //This array request is work for Child, GRE, & All MENUS
    private void makeJsonArrayRequest(String url_items) {
        showpDialog();
        JsonArrayRequest req = new JsonArrayRequest(url_items,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            jsonResponse = "";
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject obj = (JSONObject) response.get(i);

                                SubSubMenuModel ssmObj = new SubSubMenuModel();
                                if (obj.getString("id").equals("") || obj.getString("id").equals(null)) {
                                    ssmObj.setId("N/A");
                                } else {
                                    ssmObj.setId(obj.getString("id"));
                                }

                                if (obj.getString("article_cat").equals("") || obj.getString("article_cat").equals(null)) {
                                    ssmObj.setArticle_cat("N/A");
                                } else {
                                    ssmObj.setArticle_cat(obj.getString("article_cat"));
                                }

                                if (obj.getString("caption").equals("") || obj.getString("caption").equals(null)) {
                                    ssmObj.setCaption("N/A");
                                } else {
                                    ssmObj.setCaption(obj.getString("caption"));
                                }

                                if (obj.getString("description").equals("") || obj.getString("description").equals(null)) {
                                    ssmObj.setDescription("N/A");
                                } else {
                                    ssmObj.setDescription(stripHtml(obj.getString("description")));
                                }

                                subList.add(ssmObj);
                            }
//                            TastyToast.makeText(getApplicationContext(), "Touch on words for pronounciation", TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        }
                        adapter.notifyDataSetChanged();
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                hidepDialog();
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(req);
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
