package com.musabbir.englishfor2day;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.musabbir.englishfor2day.adapter.SpokenAdapter;
import com.musabbir.englishfor2day.adapter.SubSubMenuAdapter;
import com.musabbir.englishfor2day.model.SpokenModel;
import com.musabbir.englishfor2day.model.SubSubMenuModel;
import com.sdsmdg.tastytoast.TastyToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by MUSABBIR MAMUN on 19-Sep-16.
 */

//This class also works for Grammar menu as well as Spoken menu
public class Spoken extends Activity {

    private List<SpokenModel> spknList = new ArrayList<SpokenModel>();
    public ListView listView;
    private SpokenAdapter adapter;
    private ProgressDialog pDialog;
    String jsonResponse = "";
    public static String url_items = "", sub_menu_name = "";
    String strDesc = "", strCap = "", strYt = "";
    private int MY_SOCKET_TIMEOUT_MS = 15000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spoken);

        Bundle extras = getIntent().getExtras();
        url_items = extras.getString("sub_menu_url");
        sub_menu_name = extras.getString("sub_menu_name");

        listView = (ListView) findViewById(R.id.listView);

//The last parameter getBaseContext() is so important here to send the Application context to SubSubMenuAdapter class
        adapter = new SpokenAdapter(this, spknList, getBaseContext());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView artName = (TextView) view.findViewById(R.id.caption);
                strCap = artName.getText().toString();

                TextView desc = (TextView) view.findViewById(R.id.dtl);
                strDesc = desc.getText().toString();

                TextView youTv = (TextView) view.findViewById(R.id.youtube);
                strYt = youTv.getText().toString();

                Intent intent = new Intent(Spoken.this, ArticleDescription.class);
                intent.putExtra("sub_menu_name", sub_menu_name);
                intent.putExtra("caption", strCap);
                intent.putExtra("description", strDesc);
                intent.putExtra("youtubeLink", strYt);
                startActivity(intent);
            }
        });

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Please wait...");
        makeJsonArrayRequest(url_items);
    }


    //This array request is work for Child, GRE, & All MENUS
    private void makeJsonArrayRequest(String url_items) {
        showpDialog();
        JsonArrayRequest req = new JsonArrayRequest(url_items,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        try {
                            jsonResponse = "";
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject obj = (JSONObject) response.get(i);
                                SpokenModel smObj = new SpokenModel();

                                if (sub_menu_name.equals("Spoken")) {
                                    if (obj.getString("id").equals("") || obj.getString("id").equals(null)) {
                                        smObj.setId("N/A");
                                    } else {
                                        smObj.setId(obj.getString("id"));
                                    }

                                    if (obj.getString("article_type").equals("") || obj.getString("article_type").equals(null)) {
                                        smObj.setArticle_type("N/A");
                                    } else {
                                        smObj.setArticle_type(obj.getString("article_type"));
                                    }

                                    if (obj.getString("caption").equals("") || obj.getString("caption").equals(null)) {
                                        smObj.setCaption("N/A");
                                    } else {
                                        smObj.setCaption(obj.getString("caption"));
                                    }

                                    if (obj.getString("chapter_no").equals("") || obj.getString("chapter_no").equals(null)) {
                                        smObj.setChapter_no("N/A");
                                    } else {
                                        smObj.setChapter_no((obj.getString("chapter_no")));
                                    }

                                    if (obj.getString("chapter_title").equals("") || obj.getString("chapter_title").equals(null)) {
                                        smObj.setChapter_title("N/A");
                                    } else {
                                        smObj.setChapter_title((obj.getString("chapter_title")));
                                    }

                                    if (obj.getString("app_field").equals("") || obj.getString("app_field").equals(null)) {
                                        smObj.setApp_field("N/A");
                                    } else {
                                        smObj.setApp_field(stripHtml(obj.getString("app_field")));
                                    }

                                    if (obj.getString("dtl").equals("") || obj.getString("dtl").equals(null)) {
                                        smObj.setDtl("N/A");
                                    } else {
                                        smObj.setDtl(stripHtml(obj.getString("dtl")));
                                    }

                                    if (obj.getString("hit").equals("") || obj.getString("hit").equals(null)) {
                                        smObj.setHit("N/A");
                                    } else {
                                        smObj.setHit((obj.getString("hit")));
                                    }

                                    if (obj.getString("youtube").equals("") || obj.getString("youtube").equals(null)) {
                                        smObj.setYoutube("N/A");
                                    } else {
                                        smObj.setYoutube(stripHtml(obj.getString("youtube")));
                                    }
                                } else if (sub_menu_name.equals("Grammar")) {
                                    if (obj.getString("id").equals("") || obj.getString("id").equals(null)) {
                                        smObj.setId("N/A");
                                    } else {
                                        smObj.setId(obj.getString("id"));
                                    }

                                    if (obj.getString("chapter_title").equals("") || obj.getString("chapter_title").equals(null)) {
                                        smObj.setCaption("N/A");
                                    } else {
                                        smObj.setCaption(obj.getString("chapter_title"));
                                    }

                                    if (obj.getString("description").equals("") || obj.getString("description").equals(null)) {
                                        smObj.setDtl("N/A");
                                    } else {
                                        smObj.setDtl(stripHtml(obj.getString("description")));
                                    }
                                }

                                spknList.add(smObj);
                            }
//                            TastyToast.makeText(getApplicationContext(), "Touch on words for pronounciation", TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                        }
                        adapter.notifyDataSetChanged();
                        hidepDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                TastyToast.makeText(getApplicationContext(), "Sorry no words available yet or something went wrong", TastyToast.LENGTH_LONG, TastyToast.ERROR);
                hidepDialog();
            }
        });

        req.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(req);
    }

    public String stripHtml(String html) {
        return Html.fromHtml(html).toString();
    }

    private void showpDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hidepDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
